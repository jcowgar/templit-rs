use anyhow::{Context, Result};
use std::{fs, path::PathBuf};

use crate::TemplitError;

pub fn find_all_files(dir: &PathBuf) -> Result<Vec<PathBuf>> {
    let mut files = vec![];

    for path in fs::read_dir(dir)? {
        if let Ok(dir_entry) = path {
            if let Ok(meta) = dir_entry.metadata() {
                match meta.is_dir() {
                    true => files.extend(find_all_files(&dir_entry.path())?.into_iter()),
                    false => files.push(dir_entry.path()),
                }
            }
        }
    }

    Ok(files)
}

pub fn find_directory_in_path(dir_name: &str, paths: &[PathBuf]) -> Option<PathBuf> {
    paths.iter().map(|v| v.join(dir_name)).find(|x| x.exists())
}

fn find_dir_up_tree(name: &str) -> Result<Option<PathBuf>> {
    let mut cwd = std::env::current_dir()
        .map_err(|_| TemplitError::LocateTemplitDir)
        .with_context(|| "could not get the current working directory")?;

    loop {
        let dirname = cwd.join(name);

        // TODO: check directory permission, if not readable then keep moving
        if dirname.exists() {
            return Ok(Some(dirname));
        }

        cwd = match cwd.parent() {
            None => break,
            Some(p) => p.to_path_buf(),
        };
    }

    Ok(None)
}

/// Find any valid templit directories in a priority order. First
/// entry is given highest priority.
///
/// Order:
/// 1. User specified
/// 2. Any directory specified in TEMPLIT_DIR
/// 3. Walk up the tree looking for a .templits dir
/// 4. XDG_CONFIG/templit
pub fn find_template_dirs(user_specified: &Option<PathBuf>) -> Result<Vec<PathBuf>> {
    let mut template_dirs = Vec::new();

    if let Some(v) = user_specified {
        template_dirs.push(v.clone());
    }

    if let Ok(v) = std::env::var("TEMPLIT_DIR") {
        template_dirs.push(v.into());
    }

    if let Some(v) = find_dir_up_tree(".templits")? {
        template_dirs.push(v);
    }

    if let Some(v) = dirs::config_dir() {
        template_dirs.push(v.join("templit"));
    }

    Ok(template_dirs)
}
