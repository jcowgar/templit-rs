use std::collections::BTreeMap;
use std::fs;
use std::path::PathBuf;
use thiserror::Error;

#[derive(Debug)]
pub struct Template {
    pub filename: PathBuf,
    pub header: String,
    pub body: String,
    pub header_map: BTreeMap<String, String>,
}

impl TryFrom<PathBuf> for Template {
    fn try_from(path: PathBuf) -> Result<Self, Self::Error> {
        let raw = match fs::read_to_string(&path) {
            Ok(content) => content,
            Err(why) => return Err(TemplateError::FailedToRead(path, why.to_string())),
        };

        let (header, body) = match raw.split_once("---") {
            Some((h, b)) => (h.trim().to_owned(), b.trim_start().to_owned()),
            None => ("".to_owned(), raw),
        };

        Ok(Template {
            header_map: BTreeMap::new(),
            filename: path,
            header,
            body,
        })
    }

    type Error = TemplateError;
}

#[derive(Error, Debug)]
pub enum TemplateError {
    #[error("could not read template {0:?}: {1}")]
    FailedToRead(PathBuf, String),
}
