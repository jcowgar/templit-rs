use std::path::PathBuf;

use anyhow::{Context, Result};
use thiserror::Error;

use clap::Parser;
use templit::Config;

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about=None)]
struct Args {
    /// Set the template directory.
    #[clap(short, long)]
    templit_dir: Option<PathBuf>,

    /// Set the output directory.
    #[clap(short, long)]
    output_dir: Option<PathBuf>,

    /// Allow shell command in templates.
    #[clap(short, long)]
    allow_shell: bool,

    /// Do everything except write the output files.
    #[clap(short, long)]
    dry_run: bool,

    /// Overwrite any existing files.
    #[clap(short = 'w', long)]
    overwrite: bool,

    /// Display debug output.
    #[clap(short = 'g', long)]
    debug: bool,

    /// Master template to generate from.
    #[clap(value_parser)]
    template: String,

    /// Key/Value pairs for templates.
    #[clap(value_parser)]
    variables: Vec<String>,
}

#[derive(Error, Debug)]
enum CliError {
    #[error("key=value expected, but received '{0}'")]
    KeyValueSplitError(String),

    #[error("failed to get current working directory")]
    FailedToGetCwd,
}

fn main() -> Result<()> {
    let args = Args::parse();

    let mut cfg = Config {
        allow_shell: args.allow_shell,
        debug: args.debug,
        dry_run: args.dry_run,
        overwrite: args.overwrite,
        template_dir: args.templit_dir,
        output_dir_name: get_output_dir(args.output_dir)?,
        ..Default::default()
    };

    // Insert environment variables first, that way later values will override.
    cfg.variables.extend(std::env::vars());

    // Add any command line specified key/value pairs to the template variable hash.
    for arg in &args.variables {
        let (k, v) = arg
            .split_once('=')
            .ok_or_else(|| CliError::KeyValueSplitError(arg.to_string()))
            .with_context(|| "could not parse a key value pair as supplied on the command line")?;

        cfg.variables.insert(k.to_owned(), v.to_owned());
    }

    templit::render(cfg, &args.template)
}

fn get_output_dir(dir: Option<PathBuf>) -> Result<PathBuf> {
    if let Some(v) = dir {
        return Ok(v);
    }

    match std::env::current_dir() {
        Ok(v) => Ok(v),
        Err(_) => Err(CliError::FailedToGetCwd.into()),
    }
}
