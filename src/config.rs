use std::{collections::HashMap, path::PathBuf};

#[derive(Debug, Default)]
pub struct Config {
    pub template_dir: Option<PathBuf>,
    pub output_dir_name: PathBuf,
    pub variables: HashMap<String, String>,
    pub dry_run: bool,
    pub debug: bool,
    pub overwrite: bool,
    pub allow_shell: bool,
}
