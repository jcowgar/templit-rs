mod config;
mod error;
mod files;
mod template;
mod templit;

pub use config::Config;
pub use error::TemplitError;
pub use templit::render;
