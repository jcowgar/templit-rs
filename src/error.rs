use thiserror::Error;

#[derive(Error, Debug)]
pub enum TemplitError {
    #[error("could not locate a suitable temlits source directory")]
    LocateTemplitDir,

    #[error("no templit directory could be found")]
    NoTemplitDirectory,

    #[error("master template {0} could not be found")]
    MasterTemplateNotFound(String),
}
