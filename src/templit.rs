use std::fs::{create_dir_all, self};
use std::path::PathBuf;

use crate::files::{find_all_files, find_directory_in_path, find_template_dirs};
use crate::template::Template;
use crate::Config;
use crate::TemplitError;
use heck;

use anyhow::Result;
use chrono::Utc;
use handlebars::{handlebars_helper, Handlebars};

pub fn render(config: Config, name: &str) -> Result<()> {
    // Find all possible template directories.
    let template_dirs = find_template_dirs(&config.template_dir)?;
    if template_dirs.is_empty() {
        return Err(TemplitError::NoTemplitDirectory.into());
    }

    // Find our master template in one of the above directories.
    let master_template_dir = find_directory_in_path(name, &template_dirs)
        .ok_or_else(|| TemplitError::MasterTemplateNotFound(name.to_owned()))?;

    // Process our templates in memory.
    let mut templates = vec![];

    for file in find_all_files(&master_template_dir)? {
        let mut t = Template::try_from(file)?;
        t = process_template(&config, t)?;

        templates.push(t);
    }

    // Write the templates to disk
    for t in &templates {
        let output_filename =
            generate_output_filename(&config.output_dir_name, &master_template_dir, t);
        let output_path = output_filename.as_path();
        let parent_path = output_path.parent().unwrap();

        create_dir_all(parent_path)?;

        fs::write(output_filename, &t.body)?;
    }

    Ok(())
}

fn process_template(config: &Config, template: Template) -> Result<Template> {
    let mut hb = Handlebars::new();

    setup_handlebars(&mut hb);

    hb.register_template_string("header", &template.header)
        .expect("could not register header template");
    hb.register_template_string("body", &template.body)
        .expect("could not register body template");

    let mut t = Template { ..template };

    match hb.render("header", &config.variables) {
        Ok(v) => t.header = v,
        Err(why) => return Err(why.into()),
    };

    t.header_map = serde_yaml::from_str(&t.header)?;

    match hb.render("body", &config.variables) {
        Ok(v) => {
            t.body = v;

            Ok(t)
        }
        Err(why) => Err(why.into()),
    }
}

fn setup_handlebars(hb: &mut Handlebars) {
    // Date
    handlebars_helper!(now: |fmt: String| {
        let now_utc = Utc::now();

        now_utc.format(&fmt).to_string()
    });
    hb.register_helper("now", Box::new(now));

    // Case conversion
    handlebars_helper!(upper_case: |v:String| v.to_uppercase());
    handlebars_helper!(lower_case: |v:String| v.to_lowercase());
    handlebars_helper!(kebab_case: |v:String| heck::AsKebabCase(v).to_string());
    handlebars_helper!(lower_camel_case: |v:String| heck::AsLowerCamelCase(v).to_string());
    handlebars_helper!(pascal_case: |v:String| heck::AsPascalCase(v).to_string());
    handlebars_helper!(shouty_kebab_case: |v:String| heck::AsShoutyKebabCase(v).to_string());
    handlebars_helper!(shouty_snake_case: |v:String| heck::AsShoutySnakeCase(v).to_string());
    handlebars_helper!(shouty_snek_case: |v:String| heck::AsShoutySnekCase(v).to_string());
    handlebars_helper!(snake_case: |v:String| heck::AsSnakeCase(v).to_string());
    handlebars_helper!(snek_case: |v:String| heck::AsSnekCase(v).to_string());
    handlebars_helper!(title_case: |v:String| heck::AsTitleCase(v).to_string());
    handlebars_helper!(upper_camel_case: |v:String| heck::AsUpperCamelCase(v).to_string());

    hb.register_helper("upper_case", Box::new(upper_case));
    hb.register_helper("lower_case", Box::new(lower_case));
    hb.register_helper("kebab_case", Box::new(kebab_case));
    hb.register_helper("kebab_case", Box::new(kebab_case));
    hb.register_helper("lower_camel_case", Box::new(lower_camel_case));
    hb.register_helper("pascal_case", Box::new(pascal_case));
    hb.register_helper("shouty_kebab_case", Box::new(shouty_kebab_case));
    hb.register_helper("shouty_snake_case", Box::new(shouty_snake_case));
    hb.register_helper("shouty_snek_case", Box::new(shouty_snek_case));
    hb.register_helper("snake_case", Box::new(snake_case));
    hb.register_helper("snek_case", Box::new(snek_case));
    hb.register_helper("title_case", Box::new(title_case));
    hb.register_helper("upper_camel_case", Box::new(upper_camel_case));
}

fn generate_output_filename(output_dir: &PathBuf, template_dir: &PathBuf, t: &Template) -> PathBuf {
    let dest_path = output_dir.as_path();
    let src_path = t.filename.as_path();
    let tmpl_path = template_dir.as_path();
    let template_rel_path = src_path.strip_prefix(tmpl_path).unwrap();

    let final_path = match t.header_map.get("filename") {
        None => template_rel_path.to_owned(),
        Some(v) => {
            let base = template_rel_path.parent().unwrap();
            base.join(v)
        }
    };

    dest_path.join(final_path)
}
